extends RigidBody2D
class_name Fruits

"""
WARNING!!!
Do not forget to disable collision mask layer 1 when using
this class.
"""

const BOTTOM_MOST_COORDINATE:int = 2111

signal knife_entered (cursor_object) # see "res://object/cursor.gd"
signal knife_exited (cursor_object)
signal fruit_exploded

var fruit_name:String = "coconut"

export var explosion_res = preload("res://object/explosion.tscn")


func _init():
	add_to_group("fruit")


func explode(direction:Vector2=Vector2.UP,impact:float=500.0):
	#delete object
	#then create bits that fly off.
	queue_free()
	var explosion = explosion_res.instance()
	get_parent().add_child(explosion)
	explosion.global_position = global_position
	explosion.direction = direction
	explosion.initial_velocity = impact
	explosion.restart()
	get_tree().create_timer(explosion.lifetime).connect("timeout",explosion,"queue_free")
	emit_signal("fruit_exploded")
	# dont forget to delete the particles
	pass


func _physics_process(delta):
	if position.y > BOTTOM_MOST_COORDINATE:
		queue_free()
