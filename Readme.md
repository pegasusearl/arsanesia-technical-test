## Download binary

Download binary here:

[Google Drive](https://drive.google.com/drive/folders/1esOX0wMrVimOrjMZIgrPSZtI3C8fwAVS?usp=sharing)

This build is not optimized for size yet.

[Play on Itch.io](https://pegasusearl.itch.io/arsanesia-technical-test) | **password:** `114`


## Greetings

Hello, this is participant Andhika B Raditya.


## Breaking test rule

As you can see I am not using Unity. I am aware that I'm breaking a rule for this test and is ready to fail. but **IF** it's still interest you, we may discuss further.



## What to look

It's written in gdscript using Godot, script are `*.gd` files and similar to python. I recommend opening it with Godot so you could read the `*.tscn` files too. The game engine itself is very small.

[Download Godot](https://godotengine.org/download)

Two main scene are inside `scenes` folder. The game are mainly in `game.tscn`.

Other important gameplay related files:
	`fruits.gd`
	`fruit_*.gd`
	`cursor.gd`
	`combo_counter.gd` 


## Project diary

I am still using bad asset, it has empty spaces and can still be cropped.

For the project. I made everything based on the game design with my own interpretation. 

Below are some part that I still do not understand:

`the bonus score will be awarded when the combo is broken.`

`the next object will give x1.2 of the default score as long as the combo doesn't
break`

It sounds like these two counteract each other.
