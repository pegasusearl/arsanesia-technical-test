extends Node2D


onready var text_fx = $TextFX
onready var fruit_thrower = $FruitsFlying/FruitThrower
onready var fruit_dropper = $FruitsOnTable/FruitDropper
onready var score_viewer = $Interface/Control/ScoreViewer
onready var feedback_score_res = preload("res://object/feedback_score.tscn")

var score_multiplier:float = 1.0

signal fruit_exploded


var score_table = {
	"cabbage": 10,
	"potato": 15,
	"chili": 25,
	"eggplant": 50,
	"garlic": 80,
	"watermelon": 150,
	"coconut": 150
}

func _ready():
	yield($Slicer/Cursor,"slashing_started")
	$Interface/Control/Greeting.hide()
	set_game_active(true)


func set_game_active(status:bool):
	fruit_dropper.active = status
	fruit_thrower.active = status
	if status:
		$Interface/Control/CountdownTimer.start_counting()


func _fruit_exploded(fruit):
	var feedback_score = feedback_score_res.instance()
	text_fx.add_child(feedback_score)
	
	# Scoring
	var score = score_table[fruit.fruit_name]*score_multiplier
	feedback_score.set_text(score)
	feedback_score.global_position = fruit.global_position
	score_viewer.add_score(score)
	
	emit_signal("fruit_exploded")


func _on_CountdownTimer_times_up():
	$Interface/Control/GameEnds.notify_ending(score_viewer.score)
	$Slicer/Cursor.stop_slashing_immediately()
	$FruitsOnTable.disconnect("fruit_exploded",self,"_fruit_exploded")
	$FruitsFlying.disconnect("fruit_exploded",self,"_fruit_exploded")
	set_game_active(false)
	
	yield(get_tree().create_timer(0.1),"timeout")
	
	var all_fruits_left = get_tree().get_nodes_in_group("fruit_d")
	for node in all_fruits_left:
		if node != null:
			yield(get_tree().create_timer(0.15),"timeout")
			if not is_instance_valid(node):
				continue
			node.explode()
