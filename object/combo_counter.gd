extends Control


var combo_count:int = 0
onready var game = get_node("../../..")
onready var animation = $Path14599/AnimationPlayer
onready var combo_label = $Label
onready var reason_label = $Reason
onready var reason_animation = $Reason/AnimationPlayer

var combo_active := false

const MINIMUM_COMBO_REQUIRED:int = 5


func _on_Game_fruit_exploded():
	combo_count += 1
	if combo_count == MINIMUM_COMBO_REQUIRED:
		animation.play("spawn")
		combo_active = true
		game.score_multiplier = 1.2
	combo_label.text = str(combo_count)


func break_combo(reason:String="BREAK!!"):
	combo_count = 0
	game.score_multiplier = 1.0
	if combo_active:
		combo_active = false
		animation.play("remove")
		reason_label.text = reason
		reason_animation.play("pop")


func _on_Cursor_slashing_too_long():
	break_combo("SLASH_TOO_LONG")


func _on_TableMessUpClick_table_messed_up():
	break_combo("TABLE_MESSED_UP")


func _on_Cursor_not_hitting_anything():
	break_combo("NOT_HITTING_ANYTHING")
