extends Node2D

signal fruit_exploded(fruit)

func fruit_exploded(fruit):
	emit_signal("fruit_exploded",fruit)
