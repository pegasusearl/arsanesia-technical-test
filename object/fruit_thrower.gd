extends Node2D


var active := false


export var spawn_area_v_radius:float = 400.0
onready var timer := $Timer
onready var resource_preloader := $ResourcePreloader
onready var world := get_parent()
onready var game = owner

# these are all floats, := will assign variable type according
# to assigned value.
export var lowest_horizontal_speed := 600.0
export var highest_horizontal_speed := 900.0
export var lowest_vertical_speed := -100.0
export var highest_vertical_speed := 900.0
export var default_gravity_scale := 1.0


# how about some random spawn time?
export var minimum_spawn_cooldown := 0.2
export var maximum_spawn_cooldown := 0.8

export var spawn_coconut_chance := 0.2


func _ready():
	print("[fruit_thrower] Default gravity is ",ProjectSettings.get_setting("physics/2d/default_gravity"))


func _on_Timer_timeout():
	randomize()
	timer.start(rand_range(minimum_spawn_cooldown,maximum_spawn_cooldown))
	if active:
		spawn_fruit()


func spawn_fruit():
	randomize()
	var fruit_to_spawn = "fruit_normal"
	if randf() <= spawn_coconut_chance:
		fruit_to_spawn = "fruit_coconut"
	var new_fruit = resource_preloader.get_resource(fruit_to_spawn).instance()
	world.add_child(new_fruit)
	new_fruit.position = position
	randomize()
	new_fruit.position.y = rand_range(position.y-spawn_area_v_radius,position.y+spawn_area_v_radius)
	new_fruit.linear_velocity.x = -rand_range(lowest_horizontal_speed,highest_horizontal_speed)
	randomize()
	new_fruit.linear_velocity.y = -rand_range(lowest_vertical_speed,highest_vertical_speed)
	new_fruit.gravity_scale = default_gravity_scale
	new_fruit.connect("fruit_exploded",world,"fruit_exploded",[new_fruit])

