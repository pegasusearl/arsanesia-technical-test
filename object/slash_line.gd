extends Line2D


var current_distance:float = 0.0
var shrinking_speed:float = 4000.0

const MAXIMUM_LINE_POINTS:int = 15
const DISTANCE_PER_POLYGON:float = 20.0


func _ready():
	set_process_input(false)
	start_drawing()


func _process(delta):
	while points.size() > MAXIMUM_LINE_POINTS:
		remove_point(0)
	
	if points.size() > 1:
		var point_tail:Vector2 = get_point_position(0)
		var point_mid:Vector2 = get_point_position(1)
		
		var shrinking_this_frame:float = delta*shrinking_speed
		var tail_distance:float = point_tail.distance_to(point_mid)
		
		if tail_distance > shrinking_this_frame:
			set_point_position(0,point_tail.move_toward(point_mid,shrinking_this_frame))
		else:
			remove_point(0)
	elif not is_processing_input():
		queue_free()


func start_drawing():
	current_distance = 0.0
	points = [] as PoolVector2Array
	set_process_input(true)


func stop_drawing():
	set_process_input(false)


func _input(event):
	if event is InputEventMouseMotion:
		current_distance += Vector2.ZERO.distance_to(event.relative)
	if current_distance > DISTANCE_PER_POLYGON:
		current_distance = 0.0
		add_point(get_global_mouse_position())
