extends Button


const LOCALE_INDEX = {"en":0,"id":1}
const LOCALE_LIST = ["en","id"]
var current_locale_id := 0


func _ready():
	var current_locale = TranslationServer.get_locale()
	if not LOCALE_LIST.has(current_locale):
		current_locale = LOCALE_LIST[0]
	current_locale_id = LOCALE_INDEX[current_locale]
	TranslationServer.set_locale(LOCALE_LIST[current_locale_id])


func _on_Language_pressed():
	current_locale_id += 1
	if current_locale_id >= LOCALE_LIST.size():
		current_locale_id = 0
	TranslationServer.set_locale(LOCALE_LIST[current_locale_id])
