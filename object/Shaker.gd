extends Node


var shake_duration:float = 0.0 setget set_shake_duration
var parent


export var magnitude_on_one_second:float = 20.0
#magnitude is offset radius


func _enter_tree():
	parent = get_parent()


func _ready():
	set_process(false)


func set_shake_duration(new_value:float):
	shake_duration = new_value
	_shake_duration_updated()


func _process(delta):
	shake_duration -= delta
	var magn = magnitude_on_one_second*shake_duration
	parent.offset = Vector2(rand_range(-magn,magn),rand_range(-magn,magn))
	_shake_duration_updated()


func _shake_duration_updated():
	if shake_duration > 0.0:
		set_process(true)
	else:
		set_process(false)
		shake_duration = 0.0
		parent.offset = Vector2.ZERO
