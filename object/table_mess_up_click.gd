extends Button


onready var timer = $Timer

const MAXIMUM_CLICK_PER_SECOND:int = 4
var click:int = 0

signal table_messed_up


func _on_TableMessUpClick_button_down():
	timer.start()
	click += 1
	if click >= MAXIMUM_CLICK_PER_SECOND:
		emit_signal("table_messed_up")
		click = 0


func _on_Timer_timeout():
	click = 0


func _on_FruitsFlying_fruit_exploded(fruit):
	click = 0
	timer.start()
