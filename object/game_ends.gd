extends Popup


func notify_ending(score:int):
	var tween = $Tween
	var label = $VBoxContainer/HBoxContainer/SpongeLabel
	tween.interpolate_method(label,"integer_to_text",0,score,1.0,Tween.TRANS_LINEAR)
	tween.start()
	popup_centered()


func _on_Menu_pressed():
	get_tree().change_scene("res://scenes/main_menu.tscn")
