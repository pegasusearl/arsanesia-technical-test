extends Label


var time_left:int = 120
onready var progress_bar = $TextureProgress

signal times_up


func _ready():
	print_time()


func start_counting():
	time_left = 120
	$Timer.start(1.0)


func _on_Timer_timeout():
	time_left -= 1
	print_time()
	if time_left <= 0:
		$Timer.stop()
		emit_signal("times_up")


func print_time():
	var minutes := time_left/60
	var seconds := time_left%60
	text = str(minutes)+":"+str(seconds).pad_zeros(2)
	progress_bar.value = time_left
