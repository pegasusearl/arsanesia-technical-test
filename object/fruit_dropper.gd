extends Node2D


var active := false


onready var fruit_tapper_res = $ResourcePreloader.get_resource("fruit_tapper")
onready var world = get_parent()
onready var timer = $Timer

export var maximum_fruit_on_the_table:int = 10

export var spawn_radius_horizontal:float = 300.0
export var spawn_radius_vertical:float = 150.0

export var minimum_spawn_cooldown:float = 1.0
export var maximum_spawn_cooldown:float = 2.0

var fruit_count:int = 0

# Remember fruit tapper pivot is in top left.

func _on_Timer_timeout():
	if fruit_count > maximum_fruit_on_the_table:
		restart_timer()
	else:
		if active:
			spawn_fruit()
		restart_timer()


func restart_timer():
	randomize()
	timer.start(rand_range(minimum_spawn_cooldown,maximum_spawn_cooldown))


func modify_fruit_count(value:int):
	fruit_count += value


func spawn_fruit():
	var fruit_tapper = fruit_tapper_res.instance()
	world.add_child(fruit_tapper)
	var delta_position = Vector2.ZERO
	randomize()
	delta_position.x = rand_range(-spawn_radius_horizontal,spawn_radius_horizontal)
	randomize()
	delta_position.y = rand_range(-spawn_radius_vertical,spawn_radius_vertical)
	fruit_tapper.global_position = global_position + delta_position
	modify_fruit_count(1)
	fruit_tapper.connect("tree_exiting",self,"modify_fruit_count",[-1])
	fruit_tapper.connect("fruit_exploded",world,"fruit_exploded",[fruit_tapper])
