extends Fruits


const FRUIT_HEALTH = { # value is the number of slice needed -1.
	"eggplant":0,
	"garlic":1,
}
const RED_COLOR := Color(0.92,0.18,0.51)
const ENABLE_INVINCIBILITY_ON_HURT:bool = false
const MINIMUM_MOUSE_MOVEMENT:float = 30.0

export var rotation_speed_peak:float = 360

var knife
var health:int
onready var fruit_animation = $FruitContainer/FruitAnimation

const FRUIT_LIST := ["eggplant","garlic"]


func _init():
	add_to_group("sliceable")


func _ready():
	set_process_input(false)
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	fruit_name = FRUIT_LIST[rng.randi_range(0,FRUIT_LIST.size()-1)]
	$FruitSprite.set_fruit(fruit_name)
	$FruitSprite.rotation_speed = rand_range(-rotation_speed_peak,rotation_speed_peak)
	health = FRUIT_HEALTH[fruit_name]


func hit(cursor_object):
	health -= 1
	$FruitContainer/FruitAnimation.play("hurt")
	linear_velocity += cursor_object.linear_velocity/-8.0
	var explosion = explosion_res.instance()
	get_parent().add_child(explosion)
	explosion.global_position = global_position
	explosion.direction = cursor_object.get_direction()
	explosion.initial_velocity = cursor_object.get_velocity()/-4.0
	explosion.color = RED_COLOR
	explosion.spread = 50.0
	explosion.restart()
	get_tree().create_timer(explosion.lifetime).connect("timeout",explosion,"queue_free")


func _on_NormalFruit_knife_exited(cursor_object):
	set_process_input(false)
	#print("exited with movement ",mouse_movement)
	if mouse_movement < MINIMUM_MOUSE_MOVEMENT:
		return
	elif ENABLE_INVINCIBILITY_ON_HURT and fruit_animation.is_playing() and fruit_animation.current_animation == "hurt":
		return
	if health > 0:
		hit(cursor_object)
	else:
		explode(cursor_object.get_direction(),cursor_object.get_velocity()/-4.0)


func _on_NormalFruit_knife_entered(cursor_object):
	set_process_input(true)
	mouse_movement = 0.0


var mouse_movement:float = 0.0
func _input(event):
	if event is InputEventMouseMotion:
		mouse_movement+=event.relative.distance_to(Vector2.ZERO)
