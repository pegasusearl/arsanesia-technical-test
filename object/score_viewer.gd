extends PanelContainer


var score:int = 0
onready var amount_node = $HBoxContainer/Amount

func add_score(amount:int):
	score += amount
	amount_node.text = str(score)
	
