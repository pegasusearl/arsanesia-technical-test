extends AnimatedSprite


const fruit_texture:Dictionary = {
	"cabbage":0,
	"chili":1,
	"coconut":2,
	"eggplant":3,
	"garlic":4,
	"potato":5,
	"watermelon":6
}
var rotation_speed:float = 0.0


func set_fruit(fruit_name:String):
	if fruit_texture.has(fruit_name):
		frame = fruit_texture[fruit_name]
	else:
		print("[fruit_sprite.gd] Fruit does not exist: ",fruit_name)


func _process(delta):
	rotation_degrees += rotation_speed*delta
