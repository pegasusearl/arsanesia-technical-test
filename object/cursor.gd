extends Area2D


var slashing:bool = false
onready var slash_line_resource = preload("res://object/slash_line.tscn")
onready var parent = get_parent()

const SLASH_BOTTOM_LIMIT:float = 1500.0 # table start at y=1500px
const MAXIMUM_SLASH_DURATION:float = 3.0
const MINIMUM_SLASH_DISTANCE:float = 300.0 # used to check missing slash

var last_position:Vector2
var linear_velocity:Vector2


signal slashing_stopped
signal slashing_started
signal slashing_too_long
signal not_hitting_anything


var slash_duration:float = 0.0
var slash_distance:float = 0.0
var hitting_something:bool = false

func _physics_process(delta):
	position = get_global_mouse_position()
	linear_velocity = (last_position-position)/delta
	last_position = position
	
	if slashing:
		slash_duration += delta
		if slash_duration > MAXIMUM_SLASH_DURATION:
			slash_duration = 0.0
			emit_signal("slashing_too_long")


func _input(event):
	if not slashing:
		if Input.is_mouse_button_pressed(1) and (
				get_global_mouse_position().y < SLASH_BOTTOM_LIMIT):
				# this one is to prevent accidental slash during tap
				# if you tap in the table and drag outside, it will
				# start slashing.
			slashing = true
			#print("START SLASHING")
			var slash_line = slash_line_resource.instance()
			parent.add_child(slash_line)
			slash_line.position = Vector2.ZERO
			connect("slashing_stopped",slash_line,"stop_drawing")
			slash_line.start_drawing()
			hitting_something = false
			emit_signal("slashing_started")
		else:
			return
	else: #if slashing
		if not Input.is_mouse_button_pressed(1):
			slashing = false
			slash_duration = 0.0
			if not hitting_something and slash_distance > MINIMUM_SLASH_DISTANCE:
				emit_signal("not_hitting_anything")
			slash_distance = 0.0
			emit_signal("slashing_stopped")
		elif event is InputEventMouseMotion:
			slash_distance += Vector2.ZERO.distance_to(event.relative)


func stop_slashing_immediately():
	slashing = false
	emit_signal("slashing_stopped")
	set_process_input(false)


func _on_Cursor_body_entered(body):
	if body.is_in_group("sliceable") and slashing:
		hitting_something = true
		body.emit_signal("knife_entered",self)


func _on_Cursor_body_exited(body):
	if body.is_in_group("sliceable") and slashing:
		body.emit_signal("knife_exited",self)


func get_velocity() -> float:
	return Vector2.ZERO.distance_to(linear_velocity)


func get_direction() -> Vector2:
	return linear_velocity.normalized()
