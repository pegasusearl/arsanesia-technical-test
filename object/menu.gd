extends PopupPanel


onready var page = $TabContainer

func _on_Menu_about_to_show():
	page.current_tab = 0
	get_tree().paused = true


func _on_Button_pressed():
	get_tree().change_scene("res://scenes/main_menu.tscn")


func _on_Menu_popup_hide():
	get_tree().paused = false
