extends Fruits


var core_scratched := false
onready var explosion2_res = preload("res://object/explosion2.tscn")
onready var rotation_per_second := randf()*720.0-360
onready var coconut_sprite = $Coconut


func _ready():
	fruit_name = "coconut"


func _process(delta):
	coconut_sprite.rotation_degrees += rotation_per_second*delta


func _on_FruitCoconut_knife_entered(cursor_object):
	pass


func _on_FruitCoconut_knife_exited(cursor_object):
	if core_scratched:
		explode(cursor_object.get_direction(),cursor_object.get_velocity()/-4.0)
		var explosion = explosion2_res.instance()
		get_parent().add_child(explosion)
		explosion.global_position = global_position
		explosion.restart()
	else:
		$FruitContainer/FruitAnimation.play("shielded")
		$Coconut/Shaker.shake_duration = 0.5
		linear_velocity.y = 0.0
		linear_velocity.x = max(-100,linear_velocity.x)


func _on_Core_mouse_entered():
	if Input.is_mouse_button_pressed(1):
		core_scratched = true
