extends TouchScreenButton


onready var explosion_res = preload("res://object/explosion.tscn")
onready var explosion2_res = preload("res://object/explosion2.tscn")
onready var shaker = $Drawn/FruitContainer/Shaker
const RED_COLOR := Color(0.92,0.18,0.51)
const HALF_SIZE := Vector2(100,100)

signal fruit_exploded

var fruit_name := "cabbage"
var health:int = 1
# this is not using Fruits class because it's TouchScreenButton,
# not RigidBody
# so we need to declare this variable

const FRUIT_HEALTH = { # how many tap to chop -1
	"cabbage":1,
	"potato":2,
	"chili":3,
	"watermelon":10
}

const FRUIT_LIST = ["cabbage","potato","chili","watermelon"]


func _ready():
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	fruit_name = FRUIT_LIST[rng.randi_range(0,FRUIT_LIST.size()-1)]
	$Drawn/FruitSprite.set_fruit(fruit_name)
	health = FRUIT_HEALTH[fruit_name]


func hurt():
	shaker.shake_duration = 1.0
	var explosion = explosion_res.instance()
	explosion.spread = 45
	explosion.color = RED_COLOR
	add_child(explosion)
	explosion.position = HALF_SIZE
	explosion.restart()
	$Drawn/FruitContainer/FruitAnimation.play("hurt")


func explode():
	var explosion = explosion2_res.instance()
	get_parent().add_child(explosion)
	explosion.global_position = global_position+HALF_SIZE
	explosion.restart()
	emit_signal("fruit_exploded")
	queue_free()


func _on_FruitTapper_pressed():
	if health > 1:
		health -= 1
		hurt()
	else:
		explode()
